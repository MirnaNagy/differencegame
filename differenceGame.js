var level = 3;
window.onload = init;
function init() {
    for (var i = 0; i < level; i++) {
        var img = document.createElement("IMG");
        var X = Math.floor(Math.random() * 250);
        var Y = Math.floor(Math.random() * 560);
        img.setAttribute("src", "booo.png");
        img.setAttribute("id", "img" + i);
        img.style.marginLeft = X + 'px';
        img.style.marginTop = Y + 'px';
        img.className = "img";
        img.addEventListener("click", loseFunction);
        document.getElementById("left").appendChild(img);
        var img2 = document.createElement("IMG");
        img2.setAttribute("src", "booo.png");
        img2.setAttribute("id", "image" + i);
        img2.style.marginLeft = X + 'px';
        img2.style.marginTop = Y + 'px';
        img2.className = "img";
        document.getElementById("right").appendChild(img2);
    }
    var img3 = document.createElement("IMG");
    var X = Math.floor(Math.random() * 250);
    var Y = Math.floor(Math.random() * 500);
    img3.setAttribute("src", "booo.png");
    img3.setAttribute("id", "extra");
    img3.style.marginLeft = X + 'px';
    img3.style.marginTop = Y + 'px';
    img3.className = "img";
    img3.addEventListener("click", winFunction);
    document.getElementById("left").appendChild(img3);
}
function rmvFunction() {
    for (var i = 0; i < level; i++) {
        var element = document.getElementById("img" + i);
        element.parentNode.removeChild(element);
        var element2 = document.getElementById("image" + i);
        element2.parentNode.removeChild(element2);
    }
    var element3 = document.getElementById("extra");
    element3.parentNode.removeChild(element3);
}
function winFunction() {
    rmvFunction();
    level++;
    init();
}
function loseFunction() {
    rmvFunction();
    level = 3;
    init();
}
